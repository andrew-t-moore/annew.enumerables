﻿using System;
using System.Collections.Generic;

namespace Annew.Enumerables {

	/// <summary>
	/// Provides useful methods for generating sequences of items.
	/// </summary>
	public static class Generator {

		/// <summary>
		/// Generates an IEnumerable based on the given generator function and 
		/// number of repetitions.
		/// </summary>
		/// <typeparam name="T">The type of item</typeparam>
		/// <param name="repetitions">The number of items in the resulting IEnumerable.</param>
		/// <param name="generator">The function used to generate items.</param>
		/// <returns></returns>
		public static IEnumerable<T> Generate<T> (int repetitions, Func<T> generator) {
			for (var r = 0; r < repetitions; r++) {
				yield return generator();
			}
		}

		/// <summary>
		/// Generates an IEnumerable based on the given generator function and
		/// number of repetitions.
		/// </summary>
		/// <typeparam name="T">The type of item.</typeparam>
		/// <param name="repetitions">The number of items in the resulting IEnumerable.</param>
		/// <param name="generator">The function used to generate items. The generator takes the index as a parameter.</param>
		/// <returns></returns>
		public static IEnumerable<T> Generate<T> (int repetitions, Func<int, T> generator) {
			for (var r = 0; r < repetitions; r++) {
				yield return generator(r);
			}
		}

		/// <summary>
		/// Generates an IEnumerable based on the given generator function.
		/// </summary>
		/// <typeparam name="T">The type of item.</typeparam>
		/// <param name="whilePredicate">While this function returns false, the generator will continue yielding values.</param>
		/// <param name="generator">The function used to generate items. The generator takes the index as a parameter.</param>
		/// <returns></returns>
		public static IEnumerable<T> Generate<T> (Func<bool> whilePredicate, Func<T> generator) {
			for (var r = 0; whilePredicate(); r++) {
				yield return generator();
			}
		}
		/// <summary>
		/// Generates an IEnumerable based on the given generator function.
		/// </summary>
		/// <typeparam name="T">The type of item.</typeparam>
		/// <param name="whilePredicate">While this function returns false, the generator will continue yielding values.</param>
		/// <param name="generator">The function used to generate items. The generator takes the index as a parameter.</param>
		/// <returns></returns>
		public static IEnumerable<T> Generate<T> (Func<bool> whilePredicate, Func<int, T> generator) {
			for (var r = 0; whilePredicate(); r++) {
				yield return generator(r);
			}
		}

	}
}
