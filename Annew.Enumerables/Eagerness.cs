﻿namespace Annew.Enumerables {
    public enum Eagerness {
        Greedy = 1,
        Lazy = 2
    }
}
