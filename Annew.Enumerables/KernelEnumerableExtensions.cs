﻿using System.Collections.Generic;

namespace Annew.Enumerables {
    public static class KernelEnumerableExtensions {

        /// <summary>
        /// Produces a sequence of kernels - each kernel is an item in the source enumerable, 
        /// with a number of the preceeding and following items. For example, calling 
        /// source.Kernel(1,2) would return kernels that include the preceeding item, and 
        /// the next 2 items.
        /// </summary>
        /// <param name="source">The source enumerable</param>
        /// <param name="lookBehind">The number of preceeding items to include in the kernel</param>
        /// <param name="lookAhead">The number of following items to include in the kernel</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<Kernel<T>> Kernel<T>(this IEnumerable<T> source, int lookBehind, int lookAhead) {

            bool atEndOfIterator;
            var iterator = source.GetEnumerator();
            var memory = new Queue<T>(lookBehind);
            var buffer = new Queue<T>(lookAhead);
            T current = default(T);

            while ((atEndOfIterator = iterator.MoveNext()) || buffer.Count > 0) {

                if (!current.Equals(default(T))) {
                    if (memory.Count >= lookBehind)
                        // Got enough in the memory, get rid of the oldest item.
                        memory.Dequeue();

                    // Push the old current item into the memory.
                    memory.Enqueue(current);
                }

                // Fill the lookahead buffer.
                if (!atEndOfIterator && buffer.Count < lookAhead) {
                    do {
                        buffer.Enqueue(iterator.Current);
                    } while (buffer.Count < lookAhead && (atEndOfIterator = iterator.MoveNext()));
                }

                // Get the current item either from the buffer, or from the iterator.
                current = buffer.Count > 0
                    ? buffer.Dequeue()
                    : iterator.Current;

                var list = new List<T>(lookBehind + lookAhead + 1);
                list.AddRange(memory);
                list.Add(current);
                list.AddRange(buffer);

                yield return new Kernel<T>(list, memory.Count);
            }

        }
    }
}