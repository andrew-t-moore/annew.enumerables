﻿using System.Collections.Generic;

namespace Annew.Enumerables {

	/// <summary>
	/// The result of running Venn() on two sets.
	/// </summary>
	/// <typeparam name="TLeft">The type of the objects in the left set.</typeparam>
	/// <typeparam name="TRight">The type of the objects in the right set.</typeparam>
    public class VennResult<TLeft, TRight> {

		/// <summary>
		/// The items that were only found in the left source.
		/// </summary>
        public List<TLeft> Left { get; set; }

		/// <summary>
		/// The items that were only found in the right source.
		/// </summary>
        public List<TRight> Right { get; set; }

		/// <summary>
		/// The items that were found in both the left and right sources.
		/// </summary>
        public List<Pair> Both { get; set; }

		/// <summary>
		/// A pairing of left and right nodes, used when a node exists in
		/// both the left and right nodes.
		/// </summary>
        public class Pair {

			/// <summary>
			/// The node from the left set.
			/// </summary>
            public TLeft Left { get; set; }

			/// <summary>
			/// The node from the right set.
			/// </summary>
            public TRight Right { get; set; }
        }
    }
}