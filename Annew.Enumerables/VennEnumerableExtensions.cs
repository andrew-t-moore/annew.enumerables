﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Annew.Enumerables {

    public static class VennEnumerableExtensions {

        /// <summary>
        /// Given 2 sets of objects, determines which objects are members of the left set, the right set, and members of both sets.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="leftSource"></param>
        /// <param name="rightSource"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
		public static VennResult<T, T> Venn<T, TKey> (this IEnumerable<T> leftSource, IEnumerable<T> rightSource, Func<T, TKey> keySelector) {
			return leftSource.Venn(rightSource, keySelector, keySelector);
		}

	    /// <summary>
	    /// Given 2 sets of objects, determines which objects are members of the left set, the right set, and members of both sets.
	    /// </summary>
	    /// <typeparam name="T"></typeparam>
	    /// <typeparam name="TKey"></typeparam>
	    /// <param name="leftSource"></param>
	    /// <param name="rightSource"></param>
	    /// <param name="leftKeySelector"></param>
	    /// <param name="rightKeySelector"></param>
	    /// <returns></returns>
	    public static VennResult<TLeft, TRight> Venn<TLeft, TRight, TKey> (this IEnumerable<TLeft> leftSource, IEnumerable<TRight> rightSource, Func<TLeft, TKey> leftKeySelector, Func<TRight, TKey> rightKeySelector) {

			if (leftSource == null)
				throw new ArgumentNullException("leftSource");

			if (rightSource == null)
				throw new ArgumentNullException("rightSource");

			if (leftKeySelector == null)
				throw new ArgumentNullException("leftKeySelector");

			if (rightKeySelector == null)
				throw new ArgumentNullException("leftKeySelector");

			var inLeft = new List<TLeft>();
			var inBoth = new List<VennResult<TLeft, TRight>.Pair>();

			var rightDictionary = rightSource.ToDictionary(rightKeySelector);

			foreach (var left in leftSource) {
				var leftKey = leftKeySelector(left);

				if (rightDictionary.ContainsKey(leftKey)) {
					// In both
					var right = rightDictionary[leftKey];
					inBoth.Add(new VennResult<TLeft, TRight>.Pair { Left = left, Right = right });
					rightDictionary.Remove(leftKey);
				} else {
					// Only in left.
					inLeft.Add(left);
				}
			}

			return new VennResult<TLeft, TRight> {
				Left = inLeft,
				Both = inBoth,
				Right = rightDictionary.Values.ToList()
			};
		}


	}
}
