﻿using System;
using System.Collections.Generic;

namespace Annew.Enumerables {
	public static class BatchEnumerableExtensions {


		/// <summary>
		/// Splits an IEnumerable into a IEnumerable of IEnumerables, each one having a size less than or
		/// equal to batchSize. If one batch must be shorter than another (i.e. if items % batchSize != 0), 
		/// then the last batch will be shorter.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public static IEnumerable<List<T>> Batch<T> (this IEnumerable<T> source, int size) {

			if (size < 1)
				throw new ArgumentOutOfRangeException("size", "size must be greater than zero.");

			if (source == null)
				throw new ArgumentNullException("source");

			var batch = new List<T>(size);

			foreach (var item in source) {
				batch.Add(item);
				if (batch.Count == size) {
					// The current batch has reached the maximmum size. Return it and
					// start a new batch.
					yield return batch;
					batch = new List<T>(size);
				}
			}

			if (batch.Count > 0)
				yield return batch;
		} 

	}
}
