﻿using System.Collections.Generic;

namespace Annew.Enumerables {
    public static class BufferedEnumerableExtensions {

        /// <summary>
        /// Buffers an IEnumerable, allowing lazy, index-based access to elements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static BufferedEnumerable<T> Buffer<T>(this IEnumerable<T> source) {

            if (source.GetType() == typeof (BufferedEnumerable<T>))
                return (BufferedEnumerable<T>)source;

            return new BufferedEnumerable<T>(source);
        }
    }
}