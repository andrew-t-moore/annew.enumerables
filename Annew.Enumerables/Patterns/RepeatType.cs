﻿namespace Annew.Enumerables.Patterns {
	/// <summary>
	/// 
	/// </summary>
	public enum RepeatType {

		/// <summary>
		/// Specifies that a repeating node should try to consume as many items as possible.
		/// </summary>
		Greedy,

		/// <summary>
		/// Specifies that a repeating node should try to consume as few characters as possible.
		/// </summary>
		Lazy
	}
}