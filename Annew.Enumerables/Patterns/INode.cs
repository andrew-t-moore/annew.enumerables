﻿namespace Annew.Enumerables.Patterns {
    public interface INode<T> {
        TestResult Test (BufferedEnumerable<T> source, int start);
    }
}