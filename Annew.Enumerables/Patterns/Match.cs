﻿using System.Collections.Generic;

namespace Annew.Enumerables.Patterns {
	public class Match<T> {

		private readonly Dictionary<string, Capture<T>> _captures; 

		public Match() {
			_captures = new Dictionary<string, Capture<T>>();
		}

		public Capture<T> this[string id] {
			get {
				Capture<T> value;
				if (_captures.TryGetValue(id, out value))
					return value;

				return null;
			}
			set {
				_captures[id] = value;
			}
		} 

	}
}