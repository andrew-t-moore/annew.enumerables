﻿namespace Annew.Enumerables.Patterns {

	/// <summary>
	/// A location within a sequence.
	/// </summary>
	public class Location {
		private readonly int _start;
		private readonly int _length;

		public Location (int start, int length) {
			_start = start;
			_length = length;
		}

		/// <summary>
		/// The starting index of the location within the source enumerable.
		/// </summary>
		public int Start {
			get { return _start; }
		}

		/// <summary>
		/// The length of the sub sequence.
		/// </summary>
		public int Length {
			get { return _length; }
		}
		
		public int End {
			get { return _start + _length; }
		}

	}
}