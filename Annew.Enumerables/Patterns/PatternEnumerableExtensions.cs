﻿using System;
using System.Collections.Generic;

namespace Annew.Enumerables.Patterns {

	public static class PatternEnumerableExtensions {
		public static Match<T> Match<T> (this IEnumerable<T> source, INode<T> node) {
			if (source == null) throw new ArgumentNullException("source");
			if (node == null) throw new ArgumentNullException("node");

			var buffered = source.Buffer();
			var testResult = node.Test(buffered, 0);

			throw new NotImplementedException();
		}
	}
}
