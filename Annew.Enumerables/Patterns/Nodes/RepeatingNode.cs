﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Annew.Enumerables.Patterns.Nodes {
    public class RepeatingNode<T> : INode<T> {

        private readonly INode<T> _child;
        private readonly int _min;
        private readonly int _max;
        private readonly Eagerness _eagerness;

        public RepeatingNode(int min, int max, INode<T> child) : this(min, max, Eagerness.Greedy, child){
        }

        public RepeatingNode(int min, int max, Eagerness eagerness, INode<T> child) {

            if (child == null)
                throw new ArgumentNullException("child");

            if (min < 0)
                throw new ArgumentOutOfRangeException("min", "min must be greater than or equal to zero.");

            if (max < 0)
                throw new ArgumentOutOfRangeException("max", "max must be greater than or equal to zero.");

            if (min > max)
                throw new ArgumentOutOfRangeException("min", "min must be less than or equal to max");

            _child = child;
            _min = min;
            _max = max;
            _eagerness = eagerness;
        }

        public TestResult Test(BufferedEnumerable<T> source, int start) {
            var locations = TestInternal(source, start).Buffer();

            if (!locations.Any())
                return TestResult.Failure;

            return _eagerness == Eagerness.Greedy 
				// TODO: verify correctness of reverse() + backtracking
                ? new TestResult(true, locations.Reverse()) 
                : new TestResult(true, locations);
        }

        private IEnumerable<Location> TestInternal(BufferedEnumerable<T> source, int start) {

            var loc = start;
            var results = new Stack<TestResult>(_max);

            do {
                var result = _child.Test(source, loc);
                if (result.Success) {
                    // Success.
                    results.Push(result);
                    result.Locations.MoveNext();
                    loc = result.Locations.Current.End;

                    if (results.Count >= _min && results.Count <= _max) {
                        // We've got enough locations.
                        yield return new Location(start, loc - start);
                    }
                } else {
                    // Failed to match. Backtrack.
                    bool hasMore = false;
                    while (!hasMore && results.Count != 0) {
                        hasMore = results.Peek().Locations.MoveNext();
                        if (!hasMore) {
                            results.Pop();
                        }
                    }
                }
            } while (results.Count != 0);
        } 
    }
}
