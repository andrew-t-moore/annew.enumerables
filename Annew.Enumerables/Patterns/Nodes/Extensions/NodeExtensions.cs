﻿namespace Annew.Enumerables.Patterns.Nodes.Extensions {
    public static class NodeExtensions {

        public static OptionalNode<T> Optional<T>(this INode<T> node) {
            return new OptionalNode<T>(node);
        }

        public static RepeatingNode<T> Repeat<T>(this INode<T> node, int repeats) {
            return new RepeatingNode<T>(repeats, repeats, node);
        }

        public static RepeatingNode<T> Repeat<T>(this INode<T> node, int min, int max) {
            return new RepeatingNode<T>(min, max, node);
        }
    }
}
