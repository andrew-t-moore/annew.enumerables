﻿using System.Collections.Generic;
using System.Linq;

namespace Annew.Enumerables.Patterns.Nodes {
    public class ChoiceNode<T> : INode<T> {

        private readonly BufferedEnumerable<INode<T>> _children;

        public ChoiceNode(IEnumerable<INode<T>> children) {
            _children = children.Buffer();
        }

        public ChoiceNode(params INode<T>[] children) : this((IEnumerable<INode<T>>)children) {
        }

        public TestResult Test (BufferedEnumerable<T> source, int start) {

            var childLocs = TestInternal(source, start).Buffer();
            if (childLocs.Any())
                return new TestResult(true, childLocs);

            return TestResult.Failure;
        }

        private IEnumerable<Location> TestInternal(BufferedEnumerable<T> source, int start) {
            foreach (var child in _children) {
                var childResult = child.Test(source, start);
                if (childResult.Success) {
                    while (childResult.Locations.MoveNext())
                        yield return childResult.Locations.Current;
                }
            }
        }
    }
}