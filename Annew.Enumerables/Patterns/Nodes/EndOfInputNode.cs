﻿namespace Annew.Enumerables.Patterns.Nodes {
    /// <summary>
    /// A node that only matches the end of the input.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EndOfInputNode<T> : INode<T> {

		/// <summary>
		/// Tests that the source is at the end of the input.
		/// </summary>
		/// <param name="source">The input.</param>
		/// <param name="start">The index of the element to test.</param>
		/// <returns></returns>
        public TestResult Test (BufferedEnumerable<T> source, int start) {
			
			if (source.AtEndOfSource && source.Length == start)
				// The source is fully enumerated, so we know that we're at the end.
				return new TestResult(true, new[] { new Location(start, 0) });

			// Try to load one more so we know if we're at the end.
			source.BufferTo(start);

            return source.AtEndOfSource
                ? new TestResult(true, new[] { new Location(start, 0) })
                : TestResult.Failure;
        }
    }
}