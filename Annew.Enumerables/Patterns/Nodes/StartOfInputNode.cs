﻿namespace Annew.Enumerables.Patterns.Nodes {
    public class StartOfInputNode<T> : INode<T> {
        public TestResult Test (BufferedEnumerable<T> source, int start) {
            return start == 0
                ? new TestResult(true, new[] { new Location(start, 0) })
                : TestResult.Failure;
        }
    }
}