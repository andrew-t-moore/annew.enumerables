﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Annew.Enumerables.Patterns.Nodes {
    public class SequenceNode<T> : INode<T> {

        private readonly INode<T>[] _children;

        public SequenceNode (params INode<T>[] children) {

            if (children == null)
                throw new ArgumentNullException("children");

            if (children.Length == 0)
                throw new ArgumentException("children must have 1 or more elements.");

            _children = children;
        }

        public SequenceNode(IEnumerable<INode<T>> children) : this(children.ToArray()){
        }

        public TestResult Test (BufferedEnumerable<T> source, int start) {
            var locations = TestInternal(source, start).Buffer();
            return locations.Any() 
                ? new TestResult(true, locations)
                : TestResult.Failure;
        }

        private IEnumerable<Location> TestInternal (BufferedEnumerable<T> source, int start) {

            var c = 0;
            var loc = start;
            var results = new Stack<TestResult>(_children.Length);
			
            do {
                var child = _children[c];
                var result = child.Test(source, loc);
                if (result.Success) {
                    // Success, so push the result onto the stack and carry on
                    // from wherever it finished.
                    results.Push(result);
                    result.Locations.MoveNext();
                    loc = result.Locations.Current.End;
                    c++;

                    if (c == _children.Length) {
                        // We've matched everything.
                        yield return new Location(start, loc - start);

                        // Backtrack for further matches.
                        bool hasMore = false;
                        while (!hasMore && results.Count != 0) {
                            hasMore = results.Peek().Locations.MoveNext();
                            if (!hasMore) {
                                results.Pop();
                                c--;
                            }

                        }
                    }
                } else {
                    // Failed to match. Backtrack.
                    bool hasMore = false;
                    while (!hasMore && results.Count != 0) {
                        hasMore = results.Peek().Locations.MoveNext();
                        if (!hasMore) {
                            results.Pop();
                            c--;
                        }
                    }
                }
            } while (results.Count != 0);

        }
    }
}