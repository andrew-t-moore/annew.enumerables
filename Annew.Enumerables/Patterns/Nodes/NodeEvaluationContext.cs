﻿namespace Annew.Enumerables.Patterns.Nodes {
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NodeEvaluationContext<T> {
        private readonly BufferedEnumerable<T> _source;
        private readonly int _start;

        public NodeEvaluationContext(BufferedEnumerable<T> source, int start) {
            _source = source;
            _start = start;
        }

        public BufferedEnumerable<T> Source {
            get { return _source; }
        }

        public int Start {
            get { return _start; }
        }
    }
}