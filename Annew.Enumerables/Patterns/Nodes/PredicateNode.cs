﻿using System;

namespace Annew.Enumerables.Patterns.Nodes {
    public class PredicateNode<T> : INode<T> {

        private readonly Func<T,bool> _predicate;

        public PredicateNode (Func<T,bool> predicate) {
            _predicate = predicate;
        }

        public TestResult Test (BufferedEnumerable<T> source, int start) {

            if (source.AtEndOfSource)
                return TestResult.Failure;

            var value = source[start];
            return _predicate(value) 
                ? new TestResult(true, new []{ new Location(start, 1) })
                : TestResult.Failure;

        }
    }
}