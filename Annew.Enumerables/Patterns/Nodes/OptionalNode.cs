﻿namespace Annew.Enumerables.Patterns.Nodes {
    public class OptionalNode<T> : INode<T> {
        private readonly INode<T> _child;

        public OptionalNode (INode<T> child) {
            _child = child;
        }

        public TestResult Test (BufferedEnumerable<T> source, int start) {
            var result = _child.Test(source, start);
            return result.Success 
                ? result 
                : new TestResult(true, new []{ new Location(start, 0) });
        }
    }
}