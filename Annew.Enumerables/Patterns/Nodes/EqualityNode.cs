﻿using System.Collections.Generic;

namespace Annew.Enumerables.Patterns.Nodes {

	/// <summary>
	/// A node that tests objects for equality. Equality is determined
	/// using EqualityComparer&lt;T&gt;.Default.Equals.
	/// </summary>
	/// <typeparam name="T"></typeparam>
    public class EqualityNode<T> : INode<T> {

        private readonly T _test;

		/// <summary>
		/// Creates a new equality node.
		/// </summary>
		/// <param name="test"></param>
        public EqualityNode (T test) {
            _test = test;
        }

		/// <summary>
		/// Tests the node at the current index for equality.
		/// </summary>
		/// <param name="source">The input.</param>
		/// <param name="start">The index of the element to test.</param>
		/// <returns></returns>
        public TestResult Test (BufferedEnumerable<T> source, int start) {

            if (source.AtEndOfSource)
                return TestResult.Failure;

            return EqualityComparer<T>.Default.Equals(_test, source[start])
                ? new TestResult(true, new[] { new Location(start, 1) })
                : TestResult.Failure;
        }
    }
}