﻿using System.Collections.Generic;

namespace Annew.Enumerables.Patterns {
	public class Capture<T> {
		private readonly string _id;
		private readonly IEnumerable<T> _items;
		private readonly int _index;

		public Capture(string id, IEnumerable<T> items, int index) {
			_id = id;
			_items = items;
			_index = index;
		}

		/// <summary>
		/// The ID of the capture group.
		/// </summary>
		public string Id {
			get { return _id; }
		}

		/// <summary>
		/// The items captured.
		/// </summary>
		public IEnumerable<T> Items {
			get { return _items; }
		}

		/// <summary>
		/// The numeric ID of the capture group.
		/// </summary>
		public int Index {
			get { return _index; }
		}
	}
}