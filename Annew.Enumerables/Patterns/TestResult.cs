﻿using System.Collections.Generic;

namespace Annew.Enumerables.Patterns {
	
	/// <summary>
	/// The result of testing a node against a sequence.
	/// </summary>
	public class TestResult {
		private readonly bool _success;
		private readonly IEnumerator<Location> _locations;

		public TestResult (bool success, IEnumerable<Location> locations) {
			_success = success;
			_locations = locations == null ? null : locations.GetEnumerator();
		}

		/// <summary>
		/// Whether the node matched the sequence.
		/// </summary>
		public bool Success {
			get { return _success; }
		}

		/// <summary>
		/// The locations matched by the node.
		/// </summary>
		public IEnumerator<Location> Locations {
			get { return _locations; }
		}

	    public static readonly TestResult Failure = new TestResult(false, null);
	}
}