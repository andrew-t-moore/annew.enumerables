﻿using System.Collections.Generic;

namespace Annew.Enumerables {
    /// <summary>
    /// Similar to the kernels used in image processing. See here: https://en.wikipedia.org/wiki/Kernel_(image_processing)
    /// Provides a simple way to iterate over objects in an enumerable while still keeping them in context of their 
    /// neighbouring items.
    /// </summary>
    public class Kernel<T> {
        private readonly List<T> _items;
        private readonly int _offset;
        private readonly int _count;

        public Kernel(List<T> items, int offset) {
            _items = items;
            _offset = offset;
            _count = items.Count;
        }

		/// <summary>
		/// The item at the centre of the kernel.
		/// </summary>
        public T Current {
            get {
                return _items[_offset];
            }
        }

		/// <summary>
		/// Whether this kernel has a previous item.
		/// </summary>
        public bool HasPrevious {
            get {
                return _offset > 0;
            }
        }

		/// <summary>
		/// The previous item in the kernel.
		/// </summary>
        public T Previous {
            get {
                return HasPrevious ? _items[_offset - 1] : default(T);
            }
        }

		/// <summary>
		/// Whether this kernel has a next item.
		/// </summary>
        public bool HasNext {
            get {
                return _offset + 1 >= _count;
            }
        }

		/// <summary>
		/// The next item in the kernel.
		/// </summary>
        public T Next {
            get {
                return HasNext ? _items[_offset + 1] : default(T);
            }
        }

		/// <summary>
		/// Determines if this kernel has an item at the position relative to the centre
		/// of the kernel. A negative value finds items preceeding the centre. A
		/// positive value finds items following the centre.
		/// </summary>
		/// <param name="relative"></param>
		/// <returns></returns>
        public bool Has(int relative) {
            var index = _offset + relative;

            if (index < 0)
                return false;

            if (index >= _count)
                return false;

            return true;
        }

		/// <summary>
		/// Returns the items at the position relative to the centre of the kernel.
		/// A negative value finds items preceeding the centre. A positive value 
		/// finds items following the centre.
		/// </summary>
		/// <param name="relative"></param>
		/// <returns></returns>
        public T this[int relative] {
            get {
                var index = _offset + relative;

                if (index < 0)
                    return default(T);

                if (index >= _count)
                    return default(T);

                return _items[index];
            }
        }
    }
}