﻿using System;
using System.Collections.Generic;

namespace Annew.Enumerables {
	public static class SplitEnumerableExtensions {

        /// <summary>
        /// Splits an IEnumerable&lt;T&gt; into a sequence of List&lt;T&gt;s. Splits the sequence
        /// before the matched items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
		public static IEnumerable<List<T>> SplitBefore<T> (this IEnumerable<T> source, Func<T,bool> predicate) {
			var enumerator = source.GetEnumerator();
			var first = true;
			var results = new List<T>();
			while (enumerator.MoveNext()) {
				if (!first && predicate(enumerator.Current)) {
					yield return results;
					results = new List<T>();
				}
				results.Add(enumerator.Current);
				first = false;
			}

			yield return results;
		}

        /// <summary>
        /// Splits an IEnumerable&lt;T&gt; into a sequence of List&lt;T&gt;s. Splits the sequence
        /// after the matched items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
		public static IEnumerable<List<T>> SplitAfter<T> (this IEnumerable<T> source, Func<T, bool> predicate) {
			var enumerator = source.GetEnumerator();
			var results = new List<T>();
			while (enumerator.MoveNext()) {
				results.Add(enumerator.Current);
				if (predicate(enumerator.Current)) {
					yield return results;
					results = new List<T>();
				}
			}

			if (results.Count > 0)
				yield return results;
		}

        /// <summary>
        /// Splits an IEnumerable&lt;T&gt; into a sequence of List&lt;T&gt;s. Splits on the matched item, 
        /// optionally discarding the matched item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <param name="discardMatchedItem">If true, the matched item is discarded. Else, the matched item
        /// is returned in its own list</param>
        /// <returns></returns>
		public static IEnumerable<List<T>> SplitOn<T> (this IEnumerable<T> source, Func<T, bool> predicate, bool discardMatchedItem = true) {
			var enumerator = source.GetEnumerator();
			var results = new List<T>();
			var first = true;
			while (enumerator.MoveNext()) {

				if (predicate(enumerator.Current)) {
					if (first) {
						// First item. Don't want to return an empty set.
					} else {
						// Return the current results.
						yield return results;

						if (!discardMatchedItem)
							// Return the matched item in its own list.
							yield return new List<T> { enumerator.Current };
						
						results = new List<T>();
					}
				} else {
					results.Add(enumerator.Current);
				}

				first = false;
			}

			if (results.Count > 0)
				yield return results;
		}
	}
}