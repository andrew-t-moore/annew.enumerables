﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Annew.Enumerables {

    // ReSharper disable once CSharpWarnings::CS1591

    /// <summary>
	/// An enumerator that allows lazy, index-based access to the underlying enumerable.
	/// The source enumerable is only ever enumerated once.
	/// </summary>
	/// <typeparam name="T">The type of items in the enumerable.</typeparam>
	public class BufferedEnumerable<T> : IEnumerable<T> {

		private readonly List<T> _buffer;
		private readonly IEnumerator<T> _source;
		private bool _atEndOfSource;

		/// <summary>
		/// Creates a new buffered enumerable that reads from the given
		/// source enumerable.
		/// </summary>
		/// <param name="source">The enumerable to read from.</param>
		public BufferedEnumerable (IEnumerable<T> source) {
			_source = source.GetEnumerator();
			_buffer = new List<T>();
			_atEndOfSource = !_source.MoveNext();
		}

		/// <summary>
		/// Retrieves the item at the given index.
		/// </summary>
		/// <param name="index">The index of the item to retrieve.</param>
		/// <exception cref="IndexOutOfRangeException">Thrown if index is less than 0, or greater than the length of the source enumerable.</exception>
		public T this[int index] {
			get {

				if (index < 0)
					throw new IndexOutOfRangeException(string.Format("Index '{0}' was out of range. Index must be greater than or equal to 0.", index));

				BufferTo(index);

				if (index < _buffer.Count)
					return _buffer[index];

				throw new IndexOutOfRangeException(string.Format("Index '{0}' was out of range. List is of length '{1}'.", index, _buffer.Count));
			}
		}

		/// <summary>
		/// Tries to buffer more items.
		/// </summary>
		/// <param name="items">The number of items to buffer.</param>
		public void Buffer (int items) {
			for (var i = 0; i < items && !_atEndOfSource; i++) {
				_buffer.Add(_source.Current);
				_atEndOfSource = !_source.MoveNext();
			}
		}

		/// <summary>
		/// Tries to buffer up to the given index in the source enumerable.
		/// </summary>
		/// <param name="index"></param>
		public void BufferTo (int index) {
			if (_buffer.Count > index)
				return;

			Buffer(1 + index - _buffer.Count);
		}

		/// <summary>
		/// Buffers all of the elements in the source enumerable.
		/// </summary>
		public void BufferAll () {
			while (!_atEndOfSource) {
				_buffer.Add(_source.Current);
				_atEndOfSource = !_source.MoveNext();
			}
		}

		/// <summary>
		/// The number of items already buffered.
		/// </summary>
		public int BufferLength {
			get { return _buffer.Count; }
		}

		/// <summary>
		/// Whether this buffered enumerable is at the end of the source enumerable.
		/// </summary>
		public bool AtEndOfSource {
			get { return _atEndOfSource; }
		}

		/// <summary>
		/// The length of the source enumerable. The length can only be determined
		/// if the entire source has been buffered. If the entire source hasn't 
		/// been buffered, null is returned.
		/// </summary>
		public int? Length {
			get {
				return AtEndOfSource 
					? (int?) BufferLength 
					: null;
			}
		}

		public IEnumerator<T> GetEnumerator () {
			return new BufferedEnumerator<T>(this);
		}

		IEnumerator IEnumerable.GetEnumerator () {
			return GetEnumerator();
		}
	}

	/// <summary>
	/// An enumerator that allows lazy, index-based access to the underlying enumerable.
	/// The source enumerable is only ever enumerated once.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class BufferedEnumerator<T> : IEnumerator<T> {
		private readonly BufferedEnumerable<T> _bufferedEnumerable;
		private int _position;
		private T _current;

		/// <summary>
		/// Creates a new buffered enumerator that lazily reads from the source enumerable
		/// when required, and stores items into a list to allow for index-based retrieval.
		/// </summary>
		/// <param name="bufferedEnumerable"></param>
		public BufferedEnumerator (BufferedEnumerable<T> bufferedEnumerable) {
			_bufferedEnumerable = bufferedEnumerable;
			_position = -1;
		}

		public void Dispose () {
		}

		public bool MoveNext () {
			_position++;
			_bufferedEnumerable.BufferTo(_position);

			if (_bufferedEnumerable.BufferLength > _position) {
				_current = _bufferedEnumerable[_position];
				return true;
			}

			return false;
		}

		public void Reset () {
			_position = -1;
			_current = default(T);
		}

		public T Current {
			get { return _current; }
		}

		object IEnumerator.Current {
			get { return Current; }
		}
	}
}
