using System;
using System.Collections.Generic;
using System.Linq;

namespace Annew.Enumerables {
    /// <summary>
    /// 
    /// </summary>
    public static class HashSetEnumerableExtensions {

        /// <summary>
        /// Produces a HashSet&gt;T&lt; from an IEnumerable&gt;T&lt;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source) {
            return new HashSet<T>(source);
        }

        /// <summary>
        /// Produces a HashSet from an IEnumerable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="projection">A projection to apply to the source items</param>
        /// <returns></returns>
        public static HashSet<TOut> ToHashSet<TIn, TOut>(this IEnumerable<TIn> source, Func<TIn, TOut> projection) {
            return new HashSet<TOut>(source.Select(projection));
        }
    }
}