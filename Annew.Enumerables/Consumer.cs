﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Annew.Enumerables {

	/// <summary>
	/// Interface for an object that can view and consume items in a input stream.
	/// Consumers can peek at upcoming items or consume items, but they cannot
	/// move backwards once they have consumed characters.
	/// </summary>
	/// <typeparam name="T">The type of item in the source enumerable.</typeparam>
	public class Consumer<T> {

		private readonly IEnumerator<T> _source;
		private readonly Queue<T> _buffer;
		private bool _atEndOfSource;

		/// <summary>
		/// Initialises a consumer. The consumer begins at position 0.
		/// </summary>
		/// <param name="source"></param>
		public Consumer (IEnumerable<T> source) {
			_source = source.GetEnumerator();
			_buffer = new Queue<T>();
			_atEndOfSource = !_source.MoveNext();
		}

		/// <summary>
		/// Tries to buffer more items.
		/// </summary>
		/// <param name="items">The number of items to buffer.</param>
		private void Buffer (int items = 1) {
			for (var i = 0; i < items && !_atEndOfSource; i++) {
				_buffer.Enqueue(_source.Current);
				Advance();
			}
		}

		private bool BufferIsEmpty {
			get {
				return _buffer.Count == 0;
			}
		}

		private bool SourceIsEmpty {
			get {
				return _atEndOfSource;
			}
		}

		private void Advance () {
			_atEndOfSource = !_source.MoveNext();
		}

		private T TakeFromSource () {
			var item = _source.Current;
			Advance();
			return item;
		}

		private T TakeFromBuffer () {
			return _buffer.Dequeue();
		}

		private void TakeFromBuffer (int number, List<T> result) {
			while (!BufferIsEmpty && number-- != 0)
				result.Add(_buffer.Dequeue());
		}

		private void TakeFromSource (int number, List<T> result) {
			while (!SourceIsEmpty && number-- != 0) {
				result.Add(TakeFromSource());
			}
		}

		/// <summary>
		/// Consumes a single item from the input.
		/// </summary>
		/// <returns></returns>
		public T Consume () {
			if (!BufferIsEmpty)
				return TakeFromBuffer();

			if (!SourceIsEmpty)
				return TakeFromSource();

			throw new InvalidOperationException("Couldn't consume an item because there are no more items in the source.");
		}

		/// <summary>
		/// Consumes multiple items from the input. If there aren't enough
		/// items in the input, as many items as possible will be returned.
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		public List<T> Consume (int number) {

			var result = new List<T>(number);

			// Try to get from the buffer first.
			TakeFromBuffer(number, result);

			if (result.Count == number)
				// We have all we need from the buffer.
				return result;

			// We need to get more from the source.
			TakeFromSource(number - result.Count, result);

			return result;
		}

		/// <summary>
		/// Consumes items from the input, while they match the predicate.
		/// </summary>
		/// <param name="predicate"></param>
		/// <returns></returns>
		public List<T> ConsumeWhile (Func<T, bool> predicate) {
			var result = new List<T>();

			while (!AtEndOfSource && predicate(Peek())) {
				// Current item matches, so add it.
				result.Add(Consume());
			}
			return result;
		}

		/// <summary>
		/// Consumes items from the input, while they match the predicate, 
		/// and up to the specified limit
		/// </summary>
		/// <param name="predicate"></param>
		/// <param name="limit">The maximum number of items to consume.</param>
		/// <returns></returns>
		public List<T> ConsumeWhile (Func<T, bool> predicate, int limit) {
			var result = new List<T>(limit);

			while (limit-- != 0 && !AtEndOfSource && predicate(Peek())) {
				// Current item matches, so add it.
				result.Add(Consume());
			}
			return result;
		}

		/// <summary>
		/// Consumes all remaining items from the input.
		/// </summary>
		/// <returns></returns>
		public List<T> ConsumeAll () {
			var result = new List<T>();

			while (!AtEndOfSource) {
				result.Add(Consume());
			}
			return result;
		}

		/// <summary>
		/// Discards the specified number of items.
		/// </summary>
		/// <param name="number">The number of items to skip. Defaults to 1.</param>
		public void Skip (int number = 1) {
			Consume(number);
		}

		/// <summary>
		/// Discards items while they match the given predicate.
		/// </summary>
		/// <param name="predicate"></param>
		public void SkipWhile (Func<T, bool> predicate) {
			ConsumeWhile(predicate);
		}

		/// <summary>
		/// Discards items while they match the given predicate.
		/// </summary>
		/// <param name="predicate"></param>
		/// <param name="limit"></param>
		public void SkipWhile (Func<T, bool> predicate, int limit) {
			ConsumeWhile(predicate, limit);
		}

		/// <summary>
		/// Discards items until one matching the predicate is found.
		/// The matched item is not discarded.
		/// </summary>
		/// <param name="predicate"></param>
		public void SkipUntil (Func<T, bool> predicate) {
			ConsumeUntil(predicate);
		}

		/// <summary>
		/// Consumes items from the input, until the a matching item is found.
		/// The matched item itself is not returned.
		/// </summary>
		/// <param name="predicate"></param>
		/// <returns></returns>
		public List<T> ConsumeUntil (Func<T, bool> predicate) {
			var result = new List<T>();

			while (!AtEndOfSource && !predicate(Peek())) {
				// Predicate failed, so add this item to
				// the result...
				result.Add(Consume());
			}
			return result;
		}

		/// <summary>
		/// Whether this consumer has reached the end of the source enumerable.
		/// </summary>
		public bool AtEndOfSource {
			get {
				return BufferIsEmpty && SourceIsEmpty;
			}
		}

		/// <summary>
		/// Returns the next item in the stream, but doesn't consume it.
		/// </summary>
		/// <returns>The next item in the stream.</returns>
		public T Peek () {

			if (!BufferIsEmpty)
				// We've already got something buffered, so
				// return that.
				return _buffer.Peek();

			if (_atEndOfSource)
				// The buffer is empty and the input source is empty too.
				throw new InvalidOperationException("Consumer has reached the end of the source.");

			// Buffer the item for use later and return it.
			Buffer();
			return _buffer.Peek();
		}

		/// <summary>
		/// Returns the next items in the stream, but doesn't consume them.
		/// </summary>
		/// <param name="number">The number of items to peek at.</param>
		/// <returns></returns>
		public List<T> Peek (int number) {
			if (_buffer.Count < number)
				// Try to buffer more.
				Buffer(number - _buffer.Count);

			return _buffer.Take(number).ToList();
		}

	}
}
