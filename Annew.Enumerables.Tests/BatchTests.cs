﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {
	[TestClass]
	public class BatchTests {

		[TestMethod]
		public void BatchingAnEmptyArrayResultsInZeroBatches () {
			var input = new int[] {  };
			const int length = 50;
			var batches = input.Batch(length).ToArray();

			batches.Length.Should().Be(0);
		}

		[TestMethod]
		public void BatchingANullEnumerableThrowAnException () {
			int[] input = null;
			// ReSharper disable ExpressionIsAlwaysNull
			input.Invoking(i => i.Batch(1).ToArray())
				 .ShouldThrow<ArgumentNullException>();
			// ReSharper restore ExpressionIsAlwaysNull
		}

		[TestMethod]
		public void BatchLengthOf0ThrowsAnException () {
			var input = new int[] { };
			// ReSharper disable IteratorMethodResultIsIgnored
			input.Invoking(i => i.Batch(0).ToArray())
				 .ShouldThrow<ArgumentOutOfRangeException>();
			// ReSharper restore IteratorMethodResultIsIgnored
		}

		[TestMethod]
		public void NegativeBatchLengthOf0ThrowsAnException () {
			var input = new int[] { };
			// ReSharper disable IteratorMethodResultIsIgnored
			input.Invoking(i => i.Batch(-4).ToArray())
				 .ShouldThrow<ArgumentOutOfRangeException>();
			// ReSharper restore IteratorMethodResultIsIgnored
		}

		[TestMethod]
		public void BatchingInputOfLengthXWithBatchLengthOf1ReturnsXEnumerables () {
			var input = new [] { 1, 2, 3, 4 };
			var batches = input.Batch(1);
			var length = batches.Count();

			length.Should().Be(input.Length);
		}

		[TestMethod]
		public void BatchingInputOfLength10WithBatchLengthOf5Returns2Enumerables () {
			var input = Enumerable.Range(0, 10).ToArray();
			var batches = input.Batch(5);
			var length = batches.Count();

			length.Should().Be(2);
		}

        [TestMethod]
        public void RemainingItemsInAnEnumerableAreReturned() {
            var input = Enumerable.Range(0, 10).ToArray();
            var batches = input.Batch(8).ToList();
            var length = batches.Count();

            length.Should().Be(2);
            batches[0].Count.Should().Be(8);
            batches[1].Count.Should().Be(2);
        }
	}
}
