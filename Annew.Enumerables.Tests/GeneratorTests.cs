﻿using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {
	[TestClass]
	public class GeneratorTests {

		[TestMethod]
		public void GeneratorsCanReturnEmptySequences () {
			var seq = Generator.Generate(0, () => 1).ToList();
			seq.Should().BeEmpty();
		}

		[TestMethod]
		public void GeneratorsCanReturnASingleItem () {
			var seq = Generator.Generate(1, () => 1).ToList();
			seq.Should().HaveCount(1);
			seq[0].Should().Be(1);
		}

		[TestMethod]
		public void GeneratorsCanReturnMultipleItems () {
			var seq = Generator.Generate(3, () => 1).ToList();
			seq.Should().HaveCount(3);
		}

		[TestMethod]
		public void GeneratorFunctionsCanBePassedAnIndex () {

			var seq = Generator.Generate(2, (x) => x * 2).ToList();
			seq.Should().HaveCount(2);
			seq[0].Should().Be(0);
			seq[1].Should().Be(2);
		}

	}
}
