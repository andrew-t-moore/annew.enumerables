﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {
	[TestClass]
	public class VennTests {

        [TestMethod]
        public void ThrowsExceptionIfLeftIsNull() {
            List<int> left = null;
            var right = new List<int>();
			// ReSharper disable ExpressionIsAlwaysNull
	        left.Invoking(l => l.Venn(right, i => i))
			// ReSharper restore ExpressionIsAlwaysNull
	            .ShouldThrow<ArgumentNullException>();
        }

		[TestMethod]
		public void ThrowsExceptionIfRightIsNull () {
			var left = new List<int>();
			List<int> right = null;
			// ReSharper disable ExpressionIsAlwaysNull
			left.Invoking(l => l.Venn(right, i => i))
				// ReSharper restore ExpressionIsAlwaysNull
				.ShouldThrow<ArgumentNullException>();
		}

		[TestMethod]
		public void IfInputsAreEmptyThenOutputsAreEmpty () {
			var left = new List<int>();
			var right = new List<int>();
			var actual = left.Venn(right, i => i);

			actual.Should().NotBeNull();
			actual.Left.Should().NotBeNull();
			actual.Both.Should().NotBeNull();
			actual.Right.Should().NotBeNull();
			actual.Left.Should().BeEmpty();
			actual.Both.Should().BeEmpty();
			actual.Right.Should().BeEmpty();
		}

		[TestMethod]
		public void IfLeftInputIsEmptyThenLeftAndBothOutputsAreEmpty () {
			var left = new List<int>();
			var right = new List<int>{ 0, 1 };
			var actual = left.Venn(right, i => i);

			actual.Should().NotBeNull();
			actual.Left.Should().NotBeNull();
			actual.Both.Should().NotBeNull();
			actual.Right.Should().NotBeNull();
			actual.Left.Should().BeEmpty();
			actual.Both.Should().BeEmpty();
		}

		[TestMethod]
		public void IfRightInputIsEmptyThenRightAndBothOutputsAreEmpty () {
			var left = new List<int>{ 0, 1, };
			var right = new List<int>();
			var actual = left.Venn(right, i => i);

			actual.Should().NotBeNull();
			actual.Left.Should().NotBeNull();
			actual.Both.Should().NotBeNull();
			actual.Right.Should().NotBeNull();
			actual.Both.Should().BeEmpty();
			actual.Right.Should().BeEmpty();
		}

		[TestMethod]
		public void IfInputsAreIdenticalThenAllItemsAreInBothOutput () {
			var left = new List<int> { 0, 1 };
			var right = left;
			var actual = left.Venn(right, i => i);

			actual.Should().NotBeNull();
			actual.Left.Should().NotBeNull();
			actual.Both.Should().NotBeNull();
			actual.Right.Should().NotBeNull();
			actual.Left.Should().BeEmpty();
			actual.Both.Count.Should().Be(2);
			actual.Right.Should().BeEmpty();

			actual.Both.Count(i => i.Left == 0).Should().Be(1);
			actual.Both.Count(i => i.Left == 1).Should().Be(1);
		}

        //[TestMethod]
        //public void TestStuff () {

        //    var left = System.Linq.Enumerable.Range(1, 800);
        //    var right = System.Linq.Enumerable.Range(600, 1200);
        //    //var left = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
        //    //var right = new int[] { 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 };
        //    Func<int, int> keySelector = o => o;

        //    var iterations = 5000;

        //    var timer = new Stopwatch();

        //    for (var i = 0; i < iterations; i++) {
        //        timer.Start();
        //        var result = left.Venn(right, keySelector);
        //        //var leftKeys = left.ToDictionary(keySelector);
        //        //var righKetys = right.ToDictionary(keySelector);

        //        //var leftOnly = leftKeys.Where(l => !rightKeys.ContainsKey(l.Key)).Select(p => p.Value);
        //        //var rightOnly = rightKeys.Where(r => !leftKeys.ContainsKey(r.Key)).Select(p => p.Value);
        //        //var both = leftKeys.Keys.Intersect(rightKeys.Keys)
        //        //	.Select(p => new { Left = leftKeys[p], Right = rightKeys[p] });
        //        timer.Stop();
        //    }

        //    var elapsed = (double)timer.ElapsedTicks;
        //    var ticksPerOperation = elapsed / (double)iterations;
        //}

        
	}
}