﻿using Annew.Enumerables.Patterns.Nodes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
	[TestClass]
	public class OptionalNodeTests {

		[TestMethod]
		public void OptionalNodeReturnsInternalResultOnSuccess () {
			var node = new OptionalNode<char>(
				new EqualityNode<char>('a'));
			var source = new[] { 'a', 'b' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().BeTrue();
			result.Locations.Current.Start.Should().Be(0);
			result.Locations.Current.Length.Should().Be(1);
		}

		[TestMethod]
		public void OptionalNodeReturns0WidthSuccessOnFailureOfChild () {
			var node = new OptionalNode<char>(
				new EqualityNode<char>('a'));
			var source = new[] { 'b' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().BeTrue();
			result.Locations.Current.Start.Should().Be(0);
			result.Locations.Current.Length.Should().Be(0);
		}
	}
}