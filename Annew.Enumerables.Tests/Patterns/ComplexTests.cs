﻿using Annew.Enumerables.Patterns.Nodes;
using Annew.Enumerables.Patterns.Nodes.Extensions;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
    
    [TestClass]
    public class ComplexTests {

        [TestMethod]
        public void StartOfInputNodeMatchesStartOfInput() {

            var node = new SequenceNode<char>(
                new StartOfInputNode<char>(),
                new ChoiceNode<char>(
                    new EqualityNode<char>('.'),
                    new EqualityNode<char>(',')).Repeat(3),
                new EqualityNode<char>('?').Optional(),
                new EndOfInputNode<char>());

            var source = new[] { '.', ',', '.' }.Buffer();
            var result = node.Test(source, 0);

            result.Should().NotBeNull();
            result.Success.Should().BeTrue();
        }
    }
}
