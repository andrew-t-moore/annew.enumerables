﻿using Annew.Enumerables.Patterns.Nodes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
	[TestClass]
	public class StartOfInputNodeTests {

		[TestMethod]
		public void StartOfInputNodeMatchesStartOfInput () {
			var node = new StartOfInputNode<char>();
			var source = new[] { 'a' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
		}

		[TestMethod]
		public void StartOfInputNodeOnlyMatchesStartOfInput () {
			var node = new StartOfInputNode<char>();
			var source = new[] { 'a', 'b' };
			var result = node.Test(source.Buffer(), 1);
			result.Should().NotBeNull();
			result.Success.Should().BeFalse();
		}

		[TestMethod]
		public void StartOfInputNodeIsZeroWidth () {
			var node = new StartOfInputNode<char>();
			var source = new[] { 'a', 'b' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().Be(true);
			result.Locations.Current.Should().NotBeNull();
			result.Locations.Current.Start.Should().Be(0);
			result.Locations.Current.End.Should().Be(0);
		}

	}
}
