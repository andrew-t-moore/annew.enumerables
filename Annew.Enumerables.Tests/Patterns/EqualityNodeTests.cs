﻿using Annew.Enumerables.Patterns.Nodes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
	[TestClass]
	public class EqualityNodeTests {

		[TestMethod]
		public void EqualityNodeSucceedsWhenValuesAreEqual () {
			var node = new EqualityNode<char>('a');
			var source = new[] { 'a' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().BeTrue();
			result.Locations.Current.Start.Should().Be(0);
			result.Locations.Current.Length.Should().Be(1);
		}

		[TestMethod]
		public void EqualityNodeFailsWhenValuesAreNotEqual () {
			var node = new EqualityNode<char>('a');
			var source = new[] { 'b' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeFalse();
		}

	}
}
