﻿using Annew.Enumerables.Patterns.Nodes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
	[TestClass]
	public class PredicateNodeTests {

        [TestMethod]
        public void PredicateNodeFailsOnEmptySource() {
            var node = new PredicateNode<char>(c => c == 'a');
            var source = new char[0].Buffer();
            var result = node.Test(source, 0);
            result.Should().NotBeNull();
            result.Success.Should().BeFalse();
        }


		[TestMethod]
		public void PredicateNodeIsLength1 () {
			var node = new PredicateNode<char>(c => c == 'a');
			var source = new[] { 'a' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().BeTrue();
			result.Locations.Current.Start.Should().Be(0);
			result.Locations.Current.Length.Should().Be(1);
		}

		[TestMethod]
		public void PredicateNodeOnlyMatchesElementsThatMeetThePredicate () {
			var node = new PredicateNode<char>(c => c == 'a');
			var source = new[] { 'b' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeFalse();
		}

	}
}
