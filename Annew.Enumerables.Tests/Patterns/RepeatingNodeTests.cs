﻿using Annew.Enumerables.Patterns.Nodes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
	[TestClass]
	public class RepeatingNodeTests {

        [TestMethod]
        public void RepeatingNodeReturnsFalseIfNoNodesAreFound() {
            var node = new RepeatingNode<char>(1,2, new EqualityNode<char>('a'));
            var source = new[] { 'b' };
            var result = node.Test(source.Buffer(), 0);
            result.Should().NotBeNull();
            result.Success.Should().BeFalse();
        }

        [TestMethod]
        public void RepeatingNodeReturnsFalseIfInsufficientNodesAreFound() {
            var node = new RepeatingNode<char>(3, 5, new EqualityNode<char>('a'));
            var source = new[] { 'a', 'a', 'b' };
            var result = node.Test(source.Buffer(), 0);
            result.Should().NotBeNull();
            result.Success.Should().BeFalse();
        }

        [TestMethod]
        public void RepeatingNodeReturnsTrueIfMinNodesAreFound() {
            var node = new RepeatingNode<char>(2, 5, new EqualityNode<char>('a'));
            var source = new[] { 'a', 'a', 'b' };
            var result = node.Test(source.Buffer(), 0);
            result.Should().NotBeNull();
            result.Success.Should().BeTrue();
        }

		[TestMethod]
		public void RepeatingNodeReturnsTrueIfMaxNodesAreFound () {
			var node = new RepeatingNode<char>(1, 3,
					new EqualityNode<char>('a'));
			var source = new[] { 'a', 'a', 'a', 'c' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().BeTrue();
			result.Locations.Current.Length.Should().Be(3);
		}

        //[TestMethod]
        //public void RepeatingNodeReturnsTrueIfMaxNodesAreFound() {
        //    var node = new RepeatingNode<char>(1, 3, 
        //        new SequenceNode<char>(
        //            new EqualityNode<char>('a'),
        //            new PredicateNode<char>(e => e == 'a' || e == 'c')));
        //    var source = new[] { 'a', 'a', 'a', 'c' };
        //    var result = node.Test(source.Buffer(), 0);
        //    result.Should().NotBeNull();
        //    result.Success.Should().BeTrue();

        //    while (result.Locations.MoveNext()) {
        //        Console.WriteLine(result.Locations.Current.Length);
        //    }

        //    //result.Locations.MoveNext().Should().BeTrue();
        //    //result.Locations.Current.Start.Should().Be(0);
        //    //result.Locations.Current.End.Should().Be(3);

        //}
	}
}
