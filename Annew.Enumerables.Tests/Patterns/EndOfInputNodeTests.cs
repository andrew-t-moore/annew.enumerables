﻿using Annew.Enumerables.Patterns.Nodes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
	[TestClass]
	public class EndOfInputNodeTests {

		[TestMethod]
		public void EndOfInputNodeMatchesEndOfInput () {
			var node = new EndOfInputNode<char>();
			var source = new[] { 'a' };
			var result = node.Test(source.Buffer(), 1);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
		}

		[TestMethod]
		public void EndOfInputNodeOnlyMatchesEndOfInput () {
			var node = new EndOfInputNode<char>();
			var source = new[] { 'a', 'b' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeFalse();
		}

		[TestMethod]
		public void EndOfInputNodeIsZeroWidth () {
			var node = new EndOfInputNode<char>();
			var source = new[] { 'a', 'b' };
			var result = node.Test(source.Buffer(), 2);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().BeTrue();
			result.Locations.Current.Should().NotBeNull();
			result.Locations.Current.Start.Should().Be(2);
			result.Locations.Current.End.Should().Be(2);
		}

	}
}
