﻿using System;
using Annew.Enumerables.Patterns.Nodes;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests.Patterns {
	[TestClass]
	public class SequenceNodeTests {

		[TestMethod]
		public void SequenceNodeWithOneChild () {
			var node = new SequenceNode<char>(
				new PredicateNode<char>(c => c == 'a'));

			var source = new[] { 'a' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
		}

		[TestMethod]
		public void SequenceNodeWithOneChild2 () {
			var node = new SequenceNode<char>(
				new PredicateNode<char>(c => c == 'b'));

			var source = new[] { 'a' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeFalse();
		}

		[TestMethod]
		public void SequenceNodeWithTwoChildren () {
			var node = new SequenceNode<char>(
				new PredicateNode<char>(c => c == 'a'),
				new PredicateNode<char>(c => c == 'b'));

			var source = new[] { 'a', 'b' };
			var result = node.Test(source.Buffer(), 0);
			result.Should().NotBeNull();
			result.Success.Should().BeTrue();
			result.Locations.MoveNext().Should().Be(true);
			result.Locations.Current.Should().NotBeNull();
			result.Locations.Current.Start.Should().Be(0);
			result.Locations.Current.End.Should().Be(2);
		}

		[TestMethod]
		public void SequenceNodeIsGreedyByDefault () {
			var node = new SequenceNode<char>(
				new PredicateNode<char>(c => c == 'a'),
				new PredicateNode<char>(c => c == 'b'));

		}

	}
}
