﻿using System;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {

	[TestClass]
	public class ConsumerTests {

		[TestMethod]
		public void AtEndOfSourceShouldBeTrueForAnEmptyEnumerable () {
			var input = new int[] { };
			var consumer = new Consumer<int>(input);
			consumer.AtEndOfSource.Should().BeTrue();
		}

		[TestMethod]
		public void AtEndOfSourceShouldBeFalseForNonEmptyEnumerable () {
			var input = new[] { 1 };
			var consumer = new Consumer<int>(input);
			consumer.AtEndOfSource.Should().BeFalse();
		}

		[TestMethod]
		public void CallingPeekTwiceShouldReturnTheSameItem () {
			var input = new[] { 0, 1 };
			var consumer = new Consumer<int>(input);
			var peekResult1 = consumer.Peek();
			var peekResult2 = consumer.Peek();
			peekResult1.Should().Be(peekResult2);
		}

		[TestMethod]
		public void CallingConsumeShouldAdvanceTheConsumer () {
			var input = new[] { 0, 1 };
			var consumer = new Consumer<int>(input);
			var result1 = consumer.Consume();
			var result2 = consumer.Consume();
			result1.Should().NotBe(result2);
		}

		[TestMethod]
		public void MultipleItemsCanBeConsumed () {
			var input = new[] { 0, 1, 2 };
			var consumer = new Consumer<int>(input);
			var result = consumer.Consume(2);
			result.Should().HaveCount(2);
			result.Should().ContainInOrder(0, 1);
		}

		[TestMethod]
		public void PeekingAtAnEmptyEnumerableShouldThrowAnException () {
			var input = new int[0];
			var consumer = new Consumer<int>(input);
			consumer.Invoking(b => b.Peek())
				.ShouldThrow<InvalidOperationException>();
		}

		[TestMethod]
		public void ConsumingFromAnEmptyEnumerableShouldThrowAnException () {
			var input = new int[0];
			var consumer = new Consumer<int>(input);
			consumer.Invoking(b => b.Consume())
				.ShouldThrow<InvalidOperationException>();
		}
	}
}