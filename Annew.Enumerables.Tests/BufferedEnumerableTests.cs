﻿using System;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {
	[TestClass]
	public class BufferedEnumerableTests {

        [TestMethod]
        public void AtEndOfSourceShouldBeTrueForAnEmptyEnumerable() {
            var input = new int[] { };
            var buffer = input.Buffer();
            buffer.AtEndOfSource.Should().Be(true);
        }

        [TestMethod]
        public void AtEndOfSourceShouldBeFalseForNonEmptyEnumerable() {
            var input = new[] { 1 };
            var buffer = input.Buffer();
            buffer.AtEndOfSource.Should().Be(false);
        }

        [TestMethod]
        public void BufferLengthShouldInitiallyBeZero() {
            var input = new[] { 1 };
            var buffer = input.Buffer();
            buffer.BufferLength.Should().Be(0);
        }

        [TestMethod]
        public void BufferingAllItemsShouldConsumeAllItems() {
            var input = new[] { 1 };
            var buffer = input.Buffer();
            buffer.BufferAll();
            buffer.AtEndOfSource.Should().Be(true);
        }

        [TestMethod]
        public void CallingBufferOnceShouldBufferOneItem() {
            var input = new[] { 1 };
            var buffer = input.Buffer();
            buffer.Buffer(1);
            buffer.BufferLength.Should().Be(1);
        }

        [TestMethod]
        public void CallingBufferToShouldConsumeEnoughItemsToReachTheSpecifiedIndex() {
            var input = new[] { 1, 2 };
            var buffer = input.Buffer();
            buffer.BufferTo(0);
            buffer.BufferLength.Should().Be(1);
        }

        [TestMethod]
        public void CallingBufferToMustNotBufferAnyItemsIfTheItemsHaveAlreaadyBeenBuffered() {
            var input = new[] { 1, 2 };
            var buffer = input.Buffer();
            buffer.BufferTo(1);
            buffer.BufferTo(0);
            buffer.BufferLength.Should().Be(2);
        }

        [TestMethod]
        public void GetReturnsTheItemsAtTheCorrectIndex() {
            var input = new[] { 0, 1, 2 };
            var buffer = input.Buffer();
            buffer[0].Should().Be(0);
            buffer[1].Should().Be(1);
            buffer[2].Should().Be(2);
        }

        [TestMethod]
        public void EnumeratingMultipleTimesReturnsTheSameObjects() {
            var input = new[] {
                new object(), 
                new object(), 
                new object()
            };

            var buffer = input.Buffer();

            var actual1 = buffer.ToArray();
            var actual2 = buffer.ToArray();

            actual1.Length.Should().Be(input.Length);
            actual1.Length.Should().Be(actual2.Length);

            for (var i = 0; i < actual1.Length; i++) {
                actual1[i].Should().BeSameAs(input[i]);
                actual1[i].Should().BeSameAs(actual2[i]);
            }
        }

        [TestMethod]
        public void GetThrowsExceptionIfTheIndexIsOutOfRange() {
            const int length = 5;
            var input = Enumerable.Range(0, length);

            var buffer = input.Buffer();

            #pragma warning disable 168
            buffer.Invoking(b => { var item = b[length]; })
                .ShouldThrow<IndexOutOfRangeException>();
            #pragma warning restore 168

        }
	}
}