﻿using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {
	[TestClass]
	public class SplitTests {

		[TestMethod]
		public void SplitBeforeDoesntReturnAnEmptyEnumerableWhenTheFirstItemMatches () {
			var input = new[] { 0 };
			var results = input.SplitBefore(i => i == 0).ToArray();

			results.Length.Should().Be(1);
			results[0].Count().Should().Be(1);
		}

		[TestMethod]
		public void SplitBeforeReturnsMatchedItemsInTheNextEnumerable () {
			var input = new[] { 0, 0 };
			var results = input.SplitBefore(i => i == 0).ToArray();

			results.Length.Should().Be(2);
			results[0].Count().Should().Be(1);
			results[1].Count().Should().Be(1);
		}

		[TestMethod]
		public void SplitAfterDoesntReturnAnEmptyEnumerableWhenTheLastItemMatches () {
			var input = new[] { 0 };
			var results = input.SplitAfter(i => i == 0).ToArray();

			results.Length.Should().Be(1);
			results[0].Count().Should().Be(1);
		}

		[TestMethod]
		public void SplitOnCanDiscardMatchedItems () {
			var input = new[] { 0, 1, 2 };
			var results = input.SplitOn(i => i == 1).ToArray();

			results.Length.Should().Be(2);
			results[0].Count().Should().Be(1);
			results[1].Count().Should().Be(1);
		}

		[TestMethod]
		public void SplitOnCanIncludeMatchedItems () {
			var input = new[] { 0, 1, 2 };
			var results = input.SplitOn(i => i == 1, false).ToArray();

			results.Length.Should().Be(3);
			results[0].Count().Should().Be(1);
			results[0].ToArray()[0].Should().Be(0);
			results[1].Count().Should().Be(1);
			results[1].ToArray()[0].Should().Be(1);
			results[2].Count().Should().Be(1);
			results[2].ToArray()[0].Should().Be(2);

		}
	}
}
