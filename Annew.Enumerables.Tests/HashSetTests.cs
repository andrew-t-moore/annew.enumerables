﻿using System.Collections.Generic;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {

    [TestClass]
    public class HashSetTests {

        [TestMethod]
        public void EmptyEnumerableResultsInAnEmptyHashSet() {
            var list = new List<string>();
            var set = list.ToHashSet();
            set.Should().NotBeNull();
            set.Count.Should().Be(0);
        }

        [TestMethod]
        public void ObjectsInTheEnumerableAreInTheHashSet() {
            var list = new List<int>{ 1 };
            var set = list.ToHashSet();
            set.Should().NotBeNull();
            set.Count.Should().Be(1);
            set.Contains(1).Should().Be(true);
        }
    }
}
