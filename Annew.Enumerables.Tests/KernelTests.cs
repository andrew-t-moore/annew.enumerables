﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Annew.Enumerables.Tests {
    [TestClass]
    public class KernelTests {
        [TestMethod]
        public void CanCreateAKernelOnAValueType() {
            var input = new List<int> { 1, 2, 3, 4 };
            var kernel = input.Kernel(1, 1).First();

            kernel.Should().NotBeNull();
            kernel.Current.Should().Be(1);
        }

        [TestMethod]
        public void CreatingAKernelOnAnEmptyEnumerableReturnsAnEmptyEnumerable() {
            var input = new List<int>();
            var kernels = input.Kernel(1, 1);

            kernels.Any().Should().Be(false);
        }

        [TestMethod]
        public void CreatingAnEnumerableOfLength1ReturnsAnEnumerableOfLength1() {
            var input = new[] { 'a' };
            var kernels = input.Kernel(1, 1);

            kernels.Count().Should().Be(1);
        }

        [TestMethod]
        public void CreatingAKernelWithAZeroLookBehindWorksAsExpected() {
            var input = new[] { 'a' };
            var kernels = input.Kernel(0, 1);

            kernels.Count().Should().Be(1);
        }

        [TestMethod]
        public void CreatingAnEnumerableOfLength50ReturnsAnEnumerableOfLength50() {
            var input = Enumerable.Range(1, 50);
            var kernels = input.Kernel(1, 1);

            kernels.Count().Should().Be(50);
        }

        [TestMethod]
        public void CreatingAKernelWithALongLookaheadReturnsTheCorrectNumberOfKernels() {
            var input = Enumerable.Range(1, 50);
            var kernels = input.Kernel(1, 999);

            kernels.Count().Should().Be(50);
        }

        [TestMethod]
        public void CreatingAKernelWithALongLookBehindReturnsTheCorrectNumberOfKernels() {
            var input = Enumerable.Range(1, 50);
            var kernels = input.Kernel(999, 1);

            kernels.Count().Should().Be(50);
        }
    }
}